﻿using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    private Monster[] _monsterCount;
    private Text _text;

    private float _timeSpent=0f;
    private float _testTime = 0.3f;

    private void Awake()
    {
        _text = GetComponent<Text>();
    }


    private void Update()
    {
        _timeSpent += Time.deltaTime;
        if (_timeSpent > _testTime)
        {
            _monsterCount = FindObjectsOfType<Monster>();
            _text.text = _monsterCount.Length.ToString();
        }
    }

}
