﻿using UnityEngine;

public class KeepAliveOnLoading : MonoBehaviour
{
    private static KeepAliveOnLoading _current = null;

    private void Awake()
    {
        if (_current != null)
            Destroy(gameObject);
        else
        _current = this;
        DontDestroyOnLoad(gameObject);
    }

}
