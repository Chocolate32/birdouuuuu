﻿using UnityEngine;

public class Monster : MonoBehaviour
{
    private SoundManager _soundManager;
    [SerializeField]
    private int _baseHp;
    [SerializeField]
    private string _deathSounds;
    [SerializeField]
    private GameObject _deadMonsterParticlePrefab;
    [SerializeField]
    private string _deathTag;

    private void Awake()
    {
        _soundManager = GameObject.FindObjectOfType<SoundManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.rigidbody.tag == _deathTag)
        {
            _baseHp--;
            return;
        }
        if (collision.rigidbody.tag == "Player")
        {
            collision.gameObject.GetComponent<Bird>().DeathCall();
        }
    }

    private void FixedUpdate()
    {
        if (_baseHp <= 0 || transform.position.y<-25)
        {
            DeathCall();
        }
    }

    public void DeathCall()
    {
        Instantiate(_deadMonsterParticlePrefab, transform.position, Quaternion.identity);
        _soundManager.Play(_deathSounds);
        Destroy(gameObject);
    }
}
