﻿using UnityEngine;
using UnityEngine.UI;

public class WinConditions : MonoBehaviour
{
    private float _time;
    private GameObject[] _monsters;
    private GameObject[] _toHide;
    public static bool isLose;

    [SerializeField]
    private Button _pause;
    [SerializeField]
    private GameObject _winningScreen;
    [SerializeField]
    private GameObject _losingScreen;
    private void Awake()
    {
        isLose = false;
        _monsters = GameObject.FindGameObjectsWithTag("Monster");
        _toHide = GameObject.FindGameObjectsWithTag("HideFromMenu");
        _time = 0f;
    }


    private void Update()
    {
        if (isLose)
        {
            _losingScreen.SetActive(true);
            Time.timeScale = 0f;
            _pause.enabled = false;
            foreach (GameObject g in _toHide)
                g.SetActive(false);
        }
        foreach(GameObject g in _monsters)
        {
            if (g != null)
                return;
        }
        if (Bird.dead)
        {
            _time += Time.deltaTime;
        }
        if (_time > 1f)
        {
            Bird.dead = false;
            _winningScreen.SetActive(true);
            Time.timeScale = 0f;
            foreach (GameObject g in _toHide)
                g.SetActive(false);
            _time = 0f;
            FindObjectOfType<SoundManager>().Play("WINNING");
            _pause.enabled = false;
        }
    }

}
