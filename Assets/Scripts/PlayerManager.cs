﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private float _spawnTime = 3f;
    private float _timeSpent = 0f;

    private Vector3 _intialPosition;

    [SerializeField]
    private GameObject _bird;
    private void Awake()
    {
        _intialPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Bird.dead)
        {
            _timeSpent += Time.fixedDeltaTime;
        }
        if (_timeSpent > _spawnTime)
        {
            Bird.dead = false;
            Instantiate(_bird, _intialPosition, Quaternion.identity);
            _timeSpent = 0f;
        }
    }
}
