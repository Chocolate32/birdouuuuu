﻿using UnityEngine;

public class DestroyAfterN_Sec : MonoBehaviour
{
    private float _passedTime = 0f;

    [SerializeField]
    private float _N_Second;


    private void FixedUpdate()
    {
        _passedTime += Time.fixedDeltaTime;
        if (_passedTime > _N_Second)
            Destroy(gameObject);
    }
}
