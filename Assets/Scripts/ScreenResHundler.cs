﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenResHundler : MonoBehaviour
{
    [SerializeField]
    private Toggle _isFullScreen;

    [SerializeField]
    private Toggle _isWindowed;
    private void Awake()
    {
        _isFullScreen.isOn = Screen.fullScreen;
        _isWindowed.isOn = !Screen.fullScreen;
    }
}
