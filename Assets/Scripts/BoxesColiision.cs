﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxesColiision : MonoBehaviour
{
    private SoundManager _soundManeger;

    [SerializeField]
    private bool _mouving;
    [SerializeField]
    private string _soundName;
    private void Awake()
    {
        _soundManeger = GameObject.FindObjectOfType<SoundManager>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" || collision.collider.tag == "Monster" || (_mouving && collision.collider.tag == "Ground"))
            _soundManeger.Play(_soundName);

    }
}
