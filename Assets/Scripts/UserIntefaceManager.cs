﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class UserIntefaceManager : MonoBehaviour
{
    #region Variables Declaration
    private static int _sceneIndex = 0;
    private GameObject[] _toHide;
    private Resolution[] resolutions;

    [SerializeField]
    private AudioMixer _volume;
    [SerializeField]
    private Dropdown _resolutionDropdown;
    [SerializeField]
    private Button[] _buttons;
    [SerializeField]
    private GameObject _panel;
    [SerializeField]
    private GameObject _pauseMenu;
    [SerializeField]
    private GameObject _mainMenu;
    [SerializeField]
    private GameObject _settingsMenu;
    #endregion

    private void Awake()
    {
        resolutions = Screen.resolutions;
        _resolutionDropdown.ClearOptions();
        List<string> screenRes=new List<string>();
        int currentRes=0;
        for(int i = 0 ; i < resolutions.Length ; ++i)
        {
            screenRes.Add(resolutions[i].height+ " x " + resolutions[i].width);
            if (Screen.currentResolution.width == resolutions[i].width && Screen.currentResolution.height == resolutions[i].height)
            {
                currentRes = i;
            }
        }
        _resolutionDropdown.AddOptions(screenRes);
        _resolutionDropdown.value = currentRes;
        _resolutionDropdown.RefreshShownValue();
        _toHide = GameObject.FindGameObjectsWithTag("HideFromMenu");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Pause()
    {
        _panel.SetActive(true);
        _pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        foreach(Button b in _buttons)
        {
            b.interactable = false;
        }
        foreach (GameObject t in _toHide)
            t.SetActive(false);
    }

    public void Resume()
    {
        _panel.SetActive(false);
        _pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        foreach (Button b in _buttons)
        {
            b.interactable = true;
        }
        foreach (GameObject t in _toHide)
            t.SetActive(true);
    }

    public void Settings()
    {
        _pauseMenu.SetActive(false);
        _settingsMenu.SetActive(true);
    }

    public void BackSettings()
    {
        _settingsMenu.SetActive(false);
        _pauseMenu.SetActive(true);
    }

    public void FullScreen()
    {
        Screen.fullScreen = true;
    }
    public void Windowed()
    {
        Screen.fullScreen = false;
    }

    public void SetQuality(int index)
    {
        QualitySettings.SetQualityLevel(index);
    }

    public void SetResolution(int index)
    {
        Screen.SetResolution(resolutions[index].width, resolutions[index].height, Screen.fullScreen);
    }

    public void SetVolume(float f)
    {
        _volume.SetFloat("sfx", f);
    }

    public void SetMusic(float f)
    {
        _volume.SetFloat("music", f);
    }

    public void CallMenuSettings()
    {
        _mainMenu.SetActive(false);
        _settingsMenu.SetActive(true);
    }

    public void BackMainMenu()
    {
        _settingsMenu.SetActive(false);
        _mainMenu.SetActive(true);
    }
}
