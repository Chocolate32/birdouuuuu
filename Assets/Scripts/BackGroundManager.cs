﻿using UnityEngine;

public class BackGroundManager : MonoBehaviour
{
    private int _chooseone;
    
    [SerializeField]
    private Sprite[] _background;


    private void Start()
    {
        _chooseone = Random.Range(0, _background.Length);
        GetComponent<SpriteRenderer>().sprite = _background[_chooseone];
    }

}
