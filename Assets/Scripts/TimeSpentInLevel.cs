﻿using UnityEngine;
using UnityEngine.UI;

public class TimeSpentInLevel : MonoBehaviour
{
    private float _levelTiming;
    private string  _seconds;
    private string _milSeconds;
    private Text _text;

    private void Awake()
    {
        _levelTiming = FindObjectsOfType<Monster>().Length*15+15;
    }
    private void Start()
    {
        _text = GetComponent<Text>();
    }

    private void Update()
    {

        if (_levelTiming <= 0 && !WinConditions.isLose)
        {
            WinConditions.isLose = true;
            _levelTiming = 0f;
            _text.text = "00 : 00";
            FindObjectOfType<SoundManager>().Play("LOSING");
            return;
        }
        if (_levelTiming != 0f)
        {
            _levelTiming -= Time.deltaTime;
            string holder = _levelTiming.ToString();
            _milSeconds = "";
            _seconds = "";
            int keep = 0;
            for (int i = 0; i < 3; ++i)
            {
                if (holder[i] != '.')
                {
                    _seconds += holder[i];
                }
                else
                {
                    keep = i + 1;
                    break;
                }
            }
            _milSeconds = holder[keep].ToString() + holder[keep + 1].ToString();
            for (int i = _seconds.Length; i < 2; ++i)
                _seconds = '0' + _seconds;
            _text.text = _seconds + " : " + _milSeconds;
        }
    }


}
